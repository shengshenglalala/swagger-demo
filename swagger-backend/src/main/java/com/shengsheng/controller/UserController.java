package com.shengsheng.controller;

import com.shengsheng.pojo.Result;
import com.shengsheng.pojo.User;
import com.shengsheng.service.UserService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * description:
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:23
 */
@RestController
/**
 * @Api 修饰整个类，描述Controller的作用
 *      tags 接口说明，可以在页面中显示。是数组，可以配置多个，当配置多个的时候，在页面中会显示多个接口的信息
 *      value 该参数没什么意义，在UI界面上不显示，可以不用配置
 *      description  用于描述用户基本信息操作
 */
@Api(tags = {"用户相关"})
@RequestMapping(value = "/backend")
public class UserController {
    @Resource
    private UserService userService;

    /**
     * @ApiOperation 用于方法，描述方法的作用，每一个接口的定义
     *      value="方法的用途和作用"
     *      notes="方法的注意事项和备注"
     *      tags：说明该方法的作用，参数是个数组，可以填多个。
     *          格式：tags={"作用1","作用2"}
     *          （在这里建议不使用这个参数，会使界面看上去有点乱，前两个常用）
     */
    @ApiOperation(value = "用户查询",notes = "查询所有用户")
    @PostMapping(value ="/findAllUser")
    public Result<List<User>> findAllUser(){
        List<User> userList =  userService.findAllUser();
        return Result.of(200, "success",userList);
    }

    /**
     *@ApiParam 用于 Controller 中方法的参数说明
     *      value:参数说明
     *      required是否必须
     *      name描述参数名称
     *      defaultValue默认值
     *      allowableValues 允许的参数范围
     */
    @ApiOperation(value = "用户查询",notes = "根据id查询")
    @GetMapping(value ="/findById/{id}")
    /**
     * @ApiImplicitParams 用在方法上，组装了多个@ApiImplicitParam
     *      @ApiImplicitParam 描述具体某个参数的信息
     *          name 该参数的字段名称
     *          value 描述改字段的含义
     *          dataType 描述传参类型
     *          required 是否必须
     *          defaultValue 默认值
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", dataType = "Long", required = true, defaultValue = "1") })
    /**
     * @ApiResponses HTTP响应整体描述 组装了多个 @ApiResponse
     *      @ApiResponse 用于方法上，说明接口响应的一些信息
     *          code 响应码
     *          message 响应消息
     *          response 响应体
     */
    @ApiResponses({ @ApiResponse(code = 200, message = "success", response = Result.class),
            @ApiResponse(code = 500, message = "failure", response = Result.class)})
    public Result<User> findById(@ApiParam(value = "用户id", required = true,name = "id",defaultValue = "1",allowableValues = "range[1, 5]") @PathVariable(value = "id")Long id){
        return Result.of(200, "success",userService.findById(id));
    }
}
