package com.shengsheng.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * description:
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:24
 */
@ApiModel(value = "result", description = "统一响应结果")
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "响应状态值", example = "200")
    private Integer status = 0;
    @ApiModelProperty(value = "响应消息", example = "success")
    private String message = "";

    private T data;


    public Result(Integer status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Result(Integer status, String message) {
        this.status = status;
        this.message = message;

    }

    public Result(Integer status) {
        this.status = status;

    }

    public Result() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> Result<T> of(Integer status, String message, T data) {
        return new Result<T>(status, message, data);
    }

}
