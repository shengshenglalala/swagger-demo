package com.shengsheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:01
*/
@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2
public class SwaggerBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerBackendApplication.class, args);
    }
}