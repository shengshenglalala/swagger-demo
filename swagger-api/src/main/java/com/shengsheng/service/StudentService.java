package com.shengsheng.service;

import com.shengsheng.pojo.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * description:
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:24
 */
@Service
public class StudentService {

    public List<Student> findAllStudent(Student student) {
        Student student1 = new Student(1L,"学生1",new Date());
        Student student2 = new Student(2L,"学生2",new Date());
        List<Student> studentList = new ArrayList<>();
        studentList.add(student1);
        studentList.add(student2);
        return studentList;
    }
}
