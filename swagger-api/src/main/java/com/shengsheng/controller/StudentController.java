package com.shengsheng.controller;

import com.shengsheng.pojo.Result;
import com.shengsheng.pojo.Student;
import com.shengsheng.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * description:
 *
 * @author shengshenglalala
 * @date 2021/8/24 12:23
 */
@RestController
@Api(tags = "学生相关")
@RequestMapping(value = "/api")
public class StudentController {
    @Resource
    private StudentService studentService;
    @ApiOperation(value = "学生查询",notes = "查询所有学生")
    @PostMapping(value ="/findAllStudent")
    public Result<List<Student>> findAllStudent(Student student){
        List<Student> studentList =  studentService.findAllStudent(student);
        return Result.of(200, "success", studentList);
    }
}
