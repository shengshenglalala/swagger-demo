package com.shengsheng.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * swagger配置
 * @author shengshenglalala
 * @date 2021/5/17 16:12
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket allApi() {
        String groupName = "RestfulApi";
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .genericModelSubstitutes(ResponseEntity.class).
                        useDefaultResponseMessages(true).
                        forCodeGeneration(false)
                .select().paths(Predicates.not(PathSelectors.regex("/actuator.*"))).build().apiInfo(new ApiInfoBuilder()
                        .title("swagger-api项目所有接口")
                        .description("swagger-api项目所有接口")
                        .termsOfServiceUrl("")
                        .version("1.0.0")
                        .build());

    }
}
