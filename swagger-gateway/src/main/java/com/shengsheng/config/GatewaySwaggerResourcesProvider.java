package com.shengsheng.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author shengshenglalala
 */
@Component
@Primary
public class GatewaySwaggerResourcesProvider implements SwaggerResourcesProvider {
    @Resource
    private RouteLocator routeLocator;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    /**
     * 给各个微服务进行重命名，以便在文档中看到更人性化的名称
     */
    private static final Map<String,String> INDEX_MAP = new HashMap<>();
    static {
        INDEX_MAP.put("swagger-api","01-swagger-api");
        INDEX_MAP.put("swagger-backend","02-swagger-backend");
    }
    /**
     * 重写get方法添加swagger的数据源
     */
    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        List<Route> routes = routeLocator.getRoutes();
        //通过RouteLocator获取路由配置，遍历获取所配置服务的接口文档，这样不需要手动添加，实现动态获取
        for (Route route : routes) {
            //微服务名称
            String routeId = route.getId();
            logger.info("routeId:{}",routeId);
            String fullPath = route.getFullPath();
            logger.info("fullPath:{}",fullPath);
            String newName = INDEX_MAP.get(routeId.toLowerCase());
            //这里用于过滤我们不想展示的微服务
            // 比如假设我们有个只给后台开发人员使用的微服务这个微服务不想给前端看到就可以在这里过滤掉
            if(!StringUtils.isEmpty(newName)){
                resources.add(swaggerResource(newName, fullPath.replace("**", "v2/api-docs?group=")));
            }
        }
        resources.sort(Comparator.comparing(SwaggerResource::getName));
        return resources;
    }

    /**
     * 这个地方是结合公司项目的实际情况(公司swagger分组写死了RestfulApi 无奈摊手)
     * @param name 分组名称
     */
    private SwaggerResource swaggerResource(String name, String location) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location + "RestfulApi");
        swaggerResource.setSwaggerVersion("2.0");
        return swaggerResource;
    }
}
