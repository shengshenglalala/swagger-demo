package com.shengsheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/**
 *
 * @author shengshenglalala
 * @date 2021/8/24 11:59
*/
@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
public class SwaggerGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SwaggerGatewayApplication.class,args);
    }
}